name := """kuro-otp"""

organization := "com.ejisan"

version := "0.0.1-SNAPSHOTS"

scalaVersion := "2.13.13"

crossScalaVersions := Seq(scalaVersion.value)

scalacOptions ++= {
  Seq(
    //"-release:1.8",
    "-encoding", "UTF-8",
    "-Xlint",
    "-Xfatal-warnings",
    "-Ywarn-dead-code",
    //"-Ywarn-inaccessible",
//    "-Ywarn-infer-any",
//    "-Ywarn-nullary-override",
//    "-Ywarn-nullary-unit",
//    "-Ywarn-numeric-widen",
//    "-Ywarn-unused-import",
//    "-Ywarn-value-discard",
    "-deprecation",
    "-unchecked",
    "-feature",
    "-explaintypes"
  ) ++ (scalaVersion.value.split('.').map(_.toInt).toList match {
    case v @ 2 :: 11 :: _ =>
      Seq("-optimise")
    case v @ 2 :: major :: _ if major >= 12 =>
      Seq("-opt:l:method", "-Ywarn-extra-implicit")
    case v =>
      Nil
  })
}

scalacOptions in Compile in console := Nil

scalacOptions in Compile in doc ++= Seq(
  "-sourcepath", (baseDirectory in LocalProject("kuro-otp")).value.getAbsolutePath,
  "-doc-title", "Kuro OTP (HOTP, TOTP)")

javacOptions ++= Seq("-source", "1.8")

testOptions in Test ++= Seq(
  Tests.Argument(TestFrameworks.ScalaTest, "-oD"),
  Tests.Argument(TestFrameworks.JUnit, "-q", "-v"))

libraryDependencies ++= Seq(
  "org.scala-lang.modules" %% "scala-java8-compat" % "1.0.1",
  "commons-codec" % "commons-codec" % "1.15",
  "junit" % "junit" % "4.13.2" % Test,
  "com.novocode" % "junit-interface" % "0.11" % Test,
  "org.scalatest" %% "scalatest" % "3.2.10" % Test)

publishArtifact in (Compile, packageDoc) := false
publishArtifact in packageDoc := false
sources in (Compile,doc) := Seq.empty